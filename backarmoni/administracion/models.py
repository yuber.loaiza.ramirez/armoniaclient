from django.db import models


class Categoria(models.Model):
    titulo  = models.CharField(max_length=30 ,default="ingrese el Titulo")
    descripcion  = models.CharField(max_length= 50 ,default="ingrese el descripcion")
    class Meta:
            verbose_name = 'Categoria'
            verbose_name_plural = 'Categorias'
            # ordering = 'titulo'
    def __str__(self):
               return self.titulo


class Servicios(models.Model):
    titulo  = models.CharField(max_length=30 ,default="ingrese el Titulo")
    descripcion  = models.TextField(max_length= 50 ,default="ingrese el descripcion")
    categoria = models.ForeignKey(Categoria , on_delete=models.CASCADE ,default="")
    
    class Meta:
            verbose_name = 'Servicio'
            verbose_name_plural = 'Servicios'
            # ordering = 'titulo'
    def __str__(self):
               return self.titulo



